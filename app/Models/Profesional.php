<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profesional extends Model
{
    use HasFactory;

    // esquema de conexion
    protected $connection = 'geninc';

    // nombre de la tabla
    protected $table = 'GEN_PROFESIONAL';
}
