<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Models\Usuario;
use App\Models\Profesional;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('phpmyinfo', function () {
    phpinfo(); 
})->name('phpmyinfo');

Route::get('testbd', function () {
    /* 
        indicar el esquema y la tabla 
        (opcion sin ayuda del modelo).
    */
    // $con = DB::connection('intranet');
    // todos los usuarios
    // $data = $con->select("SELECT * FROM INT_USUARIOS");

    // solo el nombre del primer usuario.
    // $data = $con->table('int_usuarios')->select("nombre")->first();

    /* 
        al usar el modelo, este ya tiene definido a que esquema y tabla pertenece.
    */
    $usuario = Usuario::select("nombre")->first();
    $profesional = Profesional::select("nombre1_prof")->first();
    return [$usuario, $profesional];

})->name('testbd');
